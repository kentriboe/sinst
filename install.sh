#!/bin/sh
echo "Moving scripts"
cp inst /usr/local/sbin/
cp sinst /usr/local/sbin/
cp inst.conf /etc/
echo "creating first time package-list, this might take a while. Beer?"
cp makelist_inst /usr/local/sbin/
echo "Done. Your config is at /etc/inst.conf. Please run makelist_inst in case you need to update package-list later. Enjoy"
